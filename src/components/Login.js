import React, { useEffect, useState } from "react";
import axios from "axios";
import config from "../config";

export default function Login() {
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    (async () => {
      setIsLoading(true);
      setIsError(false);

      try {
        const result = await axios.get(`${config.SERVER}/login`);
        setData(result.data);
      } catch (err) {
        setIsError(true);
        console.error("Encountered network error", err);
      }

      setIsLoading(false);
    })();
  }, []);

  const spotifyAuthenticate = () => {
    window.location.href = data.url;
  };

  // Display error message
  if (isError) {
    return <div>Something went wrong...</div>;
  }

  // Display loading indicator
  if (isLoading) {
    return <div>Loading ...</div>;
  }

  // Display Spotify Login button
  return (
    <div>
      <button className="btn btn-primary" onClick={spotifyAuthenticate}>
        Login with Spotify
      </button>
    </div>
  );
}

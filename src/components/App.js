import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { Home, Login, Callback } from "./";
import logo from "../logo.svg";

import "./App.css";

export default function App() {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/callback">
              <Callback />
            </Route>
          </Switch>
        </header>
      </div>
    </Router>
  );
}

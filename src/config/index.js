const SERVER = {
  PROTOCOL: process.env.SERVER_PROTOCOL || "http",
  URL: process.env.SERVER_URL || "localhost",
  PORT: process.env.SERVER_PORT || 3000
};

const SPOTIFY = {
  KEY: process.env.CLIENT_ID,
  SECRET: process.env.CLIENT_SECRET
};
console.log();
export const config = {
  SERVER: `${SERVER.PROTOCOL}://${SERVER.URL}:${SERVER.PORT}`,
  SPOTIFY
};

export default config;
